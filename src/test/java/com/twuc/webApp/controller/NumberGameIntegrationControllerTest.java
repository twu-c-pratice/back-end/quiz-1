package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.GameHint;
import com.twuc.webApp.domain.GameStatus;
import com.twuc.webApp.domain.NumberSequence;
import com.twuc.webApp.domain.UserGameAnswer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class NumberGameIntegrationControllerTest {
    @Autowired MockMvc mockMvc;
    private String url;
    private int gameId;

    @BeforeEach
    void should_prepare_test_data() throws Exception {
        MvcResult result = mockMvc.perform(post("/api/games")).andReturn();
        url = result.getResponse().getHeader("Location");
        gameId = Integer.parseInt(url.split("/")[3]);
    }

    @Test
    void should_creat_a_game() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().is(201))
                .andExpect(header().string("Location", String.format("/api/games/%d", ++gameId)));
    }

    @Test
    void should_get_game_status() throws Exception {
        mockMvc.perform(get(String.format("/api/games/%d", gameId)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(gameId))
                .andExpect(jsonPath("$.answer").isString());
    }

    @Test
    void should_get404_if_game_not_exist() throws Exception {
        int gameId = Integer.MAX_VALUE;
        mockMvc.perform(get(String.format("/api/games/%d", gameId))).andExpect(status().is(404));
    }

    @Test
    void should_get_hint_when_patch_the_game() throws Exception {
        UserGameAnswer userGameAnswer = new UserGameAnswer("1235");
        NumberSequence userAnswerSequence = new NumberSequence(userGameAnswer.getAnswer());

        MvcResult statusResult =
                mockMvc.perform(get(String.format("/api/games/%d", gameId))).andReturn();
        String content = statusResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        GameStatus status = mapper.readValue(content, GameStatus.class);
        NumberSequence answerSequence = new NumberSequence(status.getAnswer());
        GameHint hint = answerSequence.getHint(userAnswerSequence);

        MvcResult patchResult =
                mockMvc.perform(
                                patch(String.format("/api/games/%s", gameId))
                                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                                        .content(userGameAnswer.toString()))
                        .andReturn();

        GameHint returnHint =
                mapper.readValue(patchResult.getResponse().getContentAsString(), GameHint.class);

        assertEquals(hint, returnHint);
    }

    @Test
    void should_get_404_when_patch_game_id_not_exited() throws Exception {
        int gameId = Integer.MAX_VALUE;
        UserGameAnswer userGameAnswer = new UserGameAnswer("1235");
        mockMvc.perform(
                        patch(String.format("/api/games/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(userGameAnswer.toString()))
                .andExpect(status().is(404));
    }

    @Test
    void should_get_400_when_patch_game_id_not_number() throws Exception {
        String gameStringId = "1a54";
        UserGameAnswer userGameAnswer = new UserGameAnswer("1235");
        mockMvc.perform(
                        patch(String.format("/api/games/%s", gameStringId))
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(userGameAnswer.toString()))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_400_when_answer_to_long() throws Exception {
        UserGameAnswer userGameAnswer = new UserGameAnswer("12359");
        mockMvc.perform(
                        patch(String.format("/api/games/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(userGameAnswer.toString()))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_400_when_answer_contains_not_number() throws Exception {
        UserGameAnswer userGameAnswer = new UserGameAnswer("1a35");
        mockMvc.perform(
                        patch(String.format("/api/games/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(userGameAnswer.toString()))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_400_when_answer_contains_same_number() throws Exception {
        UserGameAnswer userGameAnswer = new UserGameAnswer("1135");
        mockMvc.perform(
                        patch(String.format("/api/games/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(userGameAnswer.toString()))
                .andExpect(status().is(400));
    }
}
