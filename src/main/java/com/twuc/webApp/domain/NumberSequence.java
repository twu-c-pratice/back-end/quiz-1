package com.twuc.webApp.domain;

import java.util.ArrayList;
import java.util.List;

public class NumberSequence {
    private List<Integer> numbers = new ArrayList<>();
    private int min = 0;
    private int max = 9;

    public NumberSequence() {
        while (numbers.size() < 4) {
            int randomNum = min + (int) (Math.random() * (max - min + 1));
            if (!numbers.contains(randomNum)) {
                numbers.add(randomNum);
            }
        }
    }

    public NumberSequence(String str) {
        for (int i = 0; i < 4; i++) {
            numbers.add(Character.getNumericValue(str.charAt(i)));
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        numbers.stream().forEach(a -> result.append(a));
        return result.toString();
    }

    public int size() {
        return numbers.size();
    }

    public int getNumberByIndex(int i) {
        return numbers.get(i);
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    public GameHint getHint(NumberSequence sequence) {
        int sameCount =
                (int) numbers.stream().filter(i -> sequence.getNumbers().contains(i)).count();

        int sameIndexCount = 0;
        for (int i = 0; i < 4; i++) {
            if (numbers.get(i) == sequence.getNumbers().get(i)) {
                sameIndexCount++;
            }
        }

        Boolean isCorrect = sameIndexCount == 4;

        return new GameHint(
                String.format("%dA%dB", sameIndexCount, sameCount - sameIndexCount), isCorrect);
    }

    public GameHint getHint(String sequenceString) {
        return getHint(new NumberSequence(sequenceString));
    }
}
