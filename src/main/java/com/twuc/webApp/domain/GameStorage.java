package com.twuc.webApp.domain;

import java.util.HashMap;
import java.util.Map;

public class GameStorage {
    private Map<Integer, NumberGame> games = new HashMap<>();
    private int gameCount = 0;

    public void addGame(NumberGame game) {
        gameCount++;
        games.put(gameCount, game);
    }

    public NumberGame getGameById(int gameId) {
        return games.get(gameId);
    }

    public int getGameCount() {
        return gameCount;
    }

    public GameStatus getGameStatusById(int id) {
        NumberGame game = getGameById(id);
        GameStatus status = new GameStatus(id, game.getNumSequence().toString());
        return status;
    }
}
