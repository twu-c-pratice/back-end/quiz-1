package com.twuc.webApp.domain;

public class UserGameAnswer {
    private String answer;

    public UserGameAnswer() {}

    public UserGameAnswer(String answer) {
        this.answer = answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    @Override
    public String toString() {
        return String.format("{\"answer\":\"%s\"}", answer);
    }
}
