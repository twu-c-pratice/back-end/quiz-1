package com.twuc.webApp.domain;

import java.util.Objects;

public class GameHint {
    private String hint;
    private Boolean correct;

    public GameHint(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GameHint hint1 = (GameHint) o;
        return Objects.equals(hint, hint1.hint) && Objects.equals(correct, hint1.correct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hint, correct);
    }

    public GameHint() {}

    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    @Override
    public String toString() {
        return String.format("{\"hint\":\"%s\",\"correct\":%b}", hint, correct);
    }
}
