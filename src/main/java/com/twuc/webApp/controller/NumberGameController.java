package com.twuc.webApp.controller;

import com.twuc.webApp.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class NumberGameController {
    private GameStorage storage = new GameStorage();

    @PostMapping("/api/games")
    ResponseEntity creatNumberGame() {
        storage.addGame(new NumberGame());

        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", String.format("/api/games/%s", storage.getGameCount()))
                .build();
    }

    @GetMapping("/api/games/{id}")
    ResponseEntity getGameStatus(@PathVariable int id) {
        if (storage.getGameById(id) != null) {
            GameStatus status = storage.getGameStatusById(id);
            return ResponseEntity.status(200)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(status);
        }

        return ResponseEntity.status(404).build();
    }

    @PatchMapping("/api/games/{id}")
    ResponseEntity getGameHint(@PathVariable int id, @RequestBody UserGameAnswer answer) {
        if (storage.getGameById(id) != null) {
            NumberSequence sequence = storage.getGameById(id).getNumSequence();

            if (answer.getAnswer().length() != 4) {
                return ResponseEntity.status(400).build();
            }

            List<Integer> list = new ArrayList<>();
            char[] chars = answer.getAnswer().toCharArray();
            for (char c : chars) {
                if ((c < 48) || (c > 57)) {
                    return ResponseEntity.status(400).build();
                }
                list.add(Character.getNumericValue(c));
            }
            if (list.stream().distinct().count() != 4) {
                return ResponseEntity.status(400).build();
            }

            GameHint hint = sequence.getHint(answer.getAnswer());
            return ResponseEntity.status(200).body(hint);
        }
        return ResponseEntity.status(404).build();
    }
}
